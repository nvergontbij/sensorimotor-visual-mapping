
## MSTrial
*subclass of the 'exptools.core.trial.Trial' class*
#### At initialization:
- **Takes the following arguments:**
  - config (dictionary).
  -  all other arguments of Trial superclass.

- **Sets the following:**
  - phase_durations from 'config':
    - *Determines number of phases and the duration for each.*
    - *Negative values cause the phase to be  skipped instantaneously.*
  - self.movie_stim from its parent session (self.session.movie_stim - *order is given by parameters['stimulus']*)
  - self.fixation as instance of the 'psychopy.visual.GratingStim' class. *size is given by config['size_fixation_deg']*
#### Methods:
  - **draw()**
    - calls *draw()* method of each <stimulus object> according on the trial phase (through if statement)
    - calls *draw()* superclass (Trial) function to update screen surface.
  - **event():**
    - if ['any', 'terminate', 'keys'] is pressed:
      - stop session,
      - set stopped attributes = True for both Session and Trial
    - if ['any', 'forward', 'key'] is pressed:
      - calls *self.phase_forward* (self.phase += 1)

## MSSession
*subclass of the 'exptools.core.session.EyelinkSession' class*
#### At initialization:
- **Takes the following arguments:**
  - self.config parsed from 'default_settings.json'
  - self.index_number
- **Sets the following:**
  - create trials
#### Methods:
- **create_trials():**
  - creates self.movie_stims as list of stimulus objects
    - each stimulus object is an instance of the 'psychopy.visual.MovieStim' class.
  - sets self.trial_order to manage movies ordering
  - *from here the session can be changed to be arbitrarily long*
- **run():**
  - iterates through stimuli array and:
    - constructs the 'parameters' dict for each (*from self.trial_order*)
    - create trial as instance of the 'MSTrial' class
    - run the trial (movie)
  - self.close the session at the end (close screen, save data)
