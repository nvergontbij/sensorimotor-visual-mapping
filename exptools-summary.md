
## exptools.core.trial.Trial

#### At initialization:
- **Takes the following arguments:**
  - a generic 'parameters' dictionary {'stimulus': int(idx of stimulus in array), 'movie': int}
  - ti ('trial index', int)
  - phase_durations (in seconds, list of int)
  - tracker (eyetracker.EyeTracker object from pygaze module)
  - screen (passed by Session)
  - self.session (the session the trial belongs to, important for accessing <stimulus objects>)
- **Sets the following attributes:**
  - events = [] (empty list, what happens during trial will be appended)
  - phase = 0
  - phase_times = list of int, cumulative sum in sec for phase

#### Methods we use:
- **draw():**
  - calls flip method to update the contents of self.screen
- **run():**
  - send log and commands to Eyelink self.tracker
  - appends what's happening to self.events.
  - creates stimuli <?
  - checks the phase duration, if expired steps to the next phase.
- **key_events():**
  - sends key pressed to Eyelink tracker
  - logs key to self.events

## exptools.core.session.Session
#### At initialization:
- **Takes the following arguments:**
  - subject initials ()
  - index_number (int)
  - events (empty list)
- **Sets the following attributes:**
  - self.screen (see create_screen() method)
  - self.output_file (= 'data_directory/si_in_Y-m-d_H.M.S') where 'si' is subj. initials and in is the index number for the session.
#### Methods we use
- **create_screen():**
  - reads kwargs or config-file containing screen specifications
  - create self.screen from either pygaze or psychopy from the above specs (see file exp_config.cfg)

## exptools.core.session.EyelinkSession:
*subclass of the 'Session' class*
#### At initialization:
- **Takes the following arguments:**
  - kwarg settings for I-tracker calibration and sampling rate (again see exp_config.cfg)
- **Sets the following:**
  - passes the exp_config.cfg parsed attributes to 'pygaze.settings.ATTRIBUTE'
  - calls create_tracker() (auto-calibration if tracker_on = 1; custom if = 2)

#### Methods we use:
- **create_tracker():**
  - if eyetracker is on:
    - creates self.tracker as an instance of eyetracker.EyeTracker class (pygaze module)
  - connects to eyetracker, tells it:
    *(all the following can be set through exp_config.cfg)*
    - what types of eye movement to record
    - how to record them (heuristics, saccade_velocity_threshold, etc)
    - screen specs
