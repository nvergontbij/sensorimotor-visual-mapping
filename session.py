from exptools.core.session import EyelinkSession
from trial import MSTrial
from psychopy import clock
from psychopy.visual import ImageStim, MovieStim
import numpy as np
import os
import exptools
import json
import glob


class MSSession(EyelinkSession):

    def __init__(self, *args, **kwargs):
        super(MSSession, self).__init__(*args, **kwargs)

        config_file = os.path.join(os.path.abspath(os.getcwd()), 'default_settings.json')
        with open(config_file) as config_file:
            config = json.load(config_file)
        self.config = config

        self.create_stimuli()
        self.stopped = False

    def create_stimuli(self):

        if self.index_number == 0:
            # number of movies to show in one trial
            self.movies = [0] # , 1, 2, 3, 4]

        # comprehension list to pick index from self.movies
        movie_files = [os.path.join(os.path.abspath(os.getcwd()),
                       'video-stimuli', 'hand_right_{}.avi'.format(
                        x)) for x in self.movies]
        self.movie_stims = [MovieStim(self.screen, filename=mf, size=self.screen.size) for mf in movie_files]
        self.trial_order = np.arange(len(self.movie_stims))

    def run(self):

        for ti in self.trial_order:
            parameters = {'stimulus': ti,
                          'movie': self.movies[ti]}
            parameters.update(self.config) # need to check if this is needed

            # create trial
            trial = MSTrial(ti=ti,
                            config=self.config,
                            screen = self.screen,
                            session = self,
                            parameters = parameters,
                            tracker = self.tracker)
            trial.run()

            if self.stopped == True:
                break

        self.close()
